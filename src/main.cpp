/**
 * @file main.cpp
 *
 * @breif An implementation of the B-Tree part of the learned indices paper
 *
 * @date 1/05/2018
 * @author Ben Caine
 */

#include "utils/DataGenerators.h"
#include "RecursiveModelIndex.h"
#include <algorithm>
#include <sys/time.h>
#include <time.h>
#include "kdtree.h"
#include "kdbtree/KDBTree.h"
#include "kdbtree/Query.h"

unsigned int get_msec(void)
{
    static struct timeval timeval, first_timeval;

    gettimeofday(&timeval, 0);

    if(first_timeval.tv_sec == 0) {
        first_timeval = timeval;
        return 0;
    }
    return (timeval.tv_sec - first_timeval.tv_sec) * 1000 + (timeval.tv_usec - first_timeval.tv_usec) / 1000;
}


int main() {
    NetworkParameters firstStageParams;
    firstStageParams.batchSize = 256;
    firstStageParams.maxNumEpochs = 25000;
    firstStageParams.learningRate = 0.01;
    firstStageParams.numNeurons = 8;

    NetworkParameters secondStageParams;
    secondStageParams.batchSize = 64;
    secondStageParams.maxNumEpochs = 1000;
    secondStageParams.learningRate = 0.01;

    RecursiveModelIndex<int, int, 128> recursiveModelIndex(firstStageParams, secondStageParams, 256, 1e6);
    btree::btree_map<int, int> btreeMap;

    const size_t datasetSize = 10000;
    float maxValue = 1e4;

    int    ndim = 2, vcount = 100000;
    int    i, j, hit;
    long   leaf_id;
    float  *lb,*hb;
    char   filename[100];
    char   idxname[100];
    long   total_hit;
    KDBTreeResult result;
    KDBTree *h;
    h = new KDBTree();
    strcpy(idxname, "testidx");

    h->KDBTreeCreateRect(idxname, ndim,  sizeof(int) /*leaf size*/, 500 /*page_size*/);

    for(i=0; i<vcount; i++) {
        float  lb[ndim],hb[ndim];
        lb[0] = ((double)rand() / RAND_MAX) * 360.0 - 180;
        lb[1] = ((double)rand() / RAND_MAX) * 180.0 - 90;
        hb[0] = lb[0];
        hb[1] = lb[1];

        h->KDBTreeStoreRect(lb, hb, (char*) &i);
        if (i%1000 == 0) printf("%d\n", i);

    }

    for(i=0; i<10; i++) {
        float  lb[ndim],hb[ndim];
        lb[0] = ((double)rand() / RAND_MAX) * 330.0 - 180;
        lb[1] = ((double)rand() / RAND_MAX) * 150 - 90;
        hb[0] = lb[0]+30;
        hb[1] = lb[1]+30;

        int z = h->KDBTreeRangeQueryRect(lb, hb, &result);
        printf("%d points found in (%lf, %lf) - (%lf,%lf).\n", z, lb[0], lb[1], hb[0], hb[1]);

    }



    h->KDBTreeClose();

    /*
    int i, vcount = 1000000;

    unsigned int msec, start;

    struct kdtree *kd = kdtree_create(2, NULL);


    start = get_msec();
    for(i=0; i<vcount; i++) {
        double * p = new double[2];
        p[0] = ((double)rand() / RAND_MAX) * 360.0 - 180.0;
        p[1] = ((double)rand() / RAND_MAX) * 180.0 - 90.0;
        int z = kdtree_insert(kd, p, i, 1);
        //printf("%d\n", z);
        delete p;
    }
    msec = get_msec() - start;
    printf("%.3f sec\n", (float)msec / 1000.0);


    for (i=0;i<10;i++) {
        int *kduid, kd_found;
        double *rc = new double[4];
        rc[0] = ((double) rand() / RAND_MAX) * 300.0 - 150.0;
        rc[1] = ((double) rand() / RAND_MAX) * 120.0 - 60.0;
        rc[2] = rc[0] + 30;
        rc[3] = rc[1] + 30;
        kd_found = kdtree_rnn(kd, rc, &kduid, NULL);
        printf("%d points found in (%lf, %lf) - (%lf,%lf).\n", kd_found, rc[0], rc[1], rc[2], rc[3]);
    }
    */

    /*

    msec = get_msec() - start;
    printf("%.3f sec\n", (float)msec / 1000.0);

    start = get_msec();
    set = kd_nearest_range3(kd, 0, 0, 0, 40);
    msec = get_msec() - start;
    printf("range query returned %d items in %.5f sec\n", kd_res_size(set), (float)msec / 1000.0);
    kd_res_free(set);

    kd_free(kd);
    */


    auto values = getIntegerLognormals<int, datasetSize>(maxValue);
    for (auto val : values) {
        recursiveModelIndex.insert(val, val + 1);
        btreeMap.insert({val, val + 1});
    }

    recursiveModelIndex.train();

    std::vector<double> rmiDurations;
    std::vector<double> btreeDurations;
    for (unsigned int ii = 0; ii < datasetSize; ii += 500) {

        auto startTime = std::chrono::system_clock::now();
        auto result = recursiveModelIndex.find(values[ii]);
        auto endTime = std::chrono::system_clock::now();

        std::chrono::duration<double> duration = endTime - startTime;
        rmiDurations.push_back(duration.count());

        if (result) {
            std::cout << result.get().first << ", " << result.get().second << std::endl;
        } else {
            std::cout << "Failed to find value that should be there" << std::endl;
        }

        startTime = std::chrono::system_clock::now();
        auto btreeResult = btreeMap.find(values[ii]);
        endTime = std::chrono::system_clock::now();
        duration = endTime - startTime;
        btreeDurations.push_back(duration.count());
    }

    auto summaryStats = [](const std::vector<double> &durations) {
        double average = std::accumulate(durations.cbegin(), durations.cend() - 1, 0.0) / durations.size();
        auto minmax = std::minmax(durations.cbegin(), durations.cend() - 1);

        std::cout << "Min: " << *minmax.first << std::endl;
        std::cout << "Average: " << average << std::endl;
        std::cout << "Max: " << *minmax.second << std::endl;
    };

    std::cout << std::endl << std::endl;
    std::cout << "Recursive Model Index Timings" << std::endl;
    summaryStats(rmiDurations);

    std::cout << std::endl << std::endl;
    std::cout << "BTree Timings" << std::endl;
    summaryStats(btreeDurations);

    return 0;
}